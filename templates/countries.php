<?php
require __DIR__.'\db.php';

$geodata = $db->geo->find();
$countries = [];
foreach ($geodata as $key => $countryArray) {
	foreach ($countryArray as $key => $value) {
		if($key !="_id"){
			array_push($countries, $key);
		}
	}	
}

sort($countries);

echo json_encode($countries, JSON_UNESCAPED_UNICODE);   	
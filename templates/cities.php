<?php

require __DIR__.'\db.php';

$geodata = $db->geo->find();

$flag = false;
foreach ($geodata as $key => $countryArray) {
	foreach ($countryArray as $country => $cities) {
		if(strtolower($country) == strtolower($data['countryName'])) {
			sort($cities);
			$cities = array_keys(array_flip(array_unique($cities))); //fix to remove duplicate values
			echo json_encode($cities, JSON_UNESCAPED_UNICODE);
			$flag = true;
		}
	}	
}

if(!$flag){
	echo json_encode(array("error"=>"true", "message" => "There is no country named '".$data['countryName']."'"));
}
